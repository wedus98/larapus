@extends('layouts.app')
@section('content')

	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<ul class="breadcrumb">
					<li><a href="{{url('/home')}}">Dashboard</a></li>
					<li class="active">Books</li>
				</ul>
				<div class="panel panel-default">
					<div class="panel-heading">
						<h2 class="panel-title">Books</h2>
					</div>
				</div>
				<div class="panel-body">
					<p><a href="{{Url('admin/books/create')}}" class="btn btn-success">Add Books</a></p>
					<table class="table table-stripped" id="dataTable">
						<thead>
							<tr>
								<th>No</th>
								<th>Book Title</th>
								<th>Author</th>
								<th>Amount</th>
								<th>Action</th>
							</tr>
						</thead>
						@php($no=1)
						<tbody>
							@foreach($books as $data)
							<tr>
								<td>{{$no++}}</td>
								<td>{{$data->title}}</td>
								<td>{{$data->hasAuthor->name}}</td>
								<td>{{$data->amount}}</td>
								<td>
									{{Form::model($data,['route'=>['admin.books.destroy',$data],'method'=>'delete','class'=>'form-inline','onsubmit'=>'return confirm("Are you Sure ?")'])}}
									<a href="{{url('admin/books')}}/{{$data->id}}/edit" class="btn btn-success">Edit</a>
									{{Form::submit('Delete',['class'=>'btn btn-danger'])}}
									{{Form::close()}}
								</td>


							</tr>
							@endforeach
						</tbody>
						
					</table>
				</div>
			</div>
		</div>
	</div>


@endsection

