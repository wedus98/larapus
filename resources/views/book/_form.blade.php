<div class="form-group {{$errors->has('title') ? 'has-error' : ''}}">
	{{Form::label('title','Title',['class'=>'col-md-2 control-label'])}}
	<div class="col-md-4">
		{{Form::text('title',null,['class'=>'form-control'])}}
		{!!$errors->first('title','<p class="help-block"><strong>:message</strong> </p>') !!}
	</div>
</div>
<div class="form-group {{$errors->has('author_id') ? 'has-error' : ''}}">
	{{Form::label('author','Author',['class'=>'col-md-2 control-label'])}}
	<div class="col-md-4">
		{{Form::select('author_id',[''=>'']+App\Author::pluck('name','id')->all(),null,['id'=>'select-beast'])}}
		{!!$errors->first('author_id','<p class="help-block"><strong>:message</strong> </p>') !!}
	</div>
</div>
<div class="form-group {{$errors->has('amount') ? 'has-error' : ''}}">
	{{Form::label('amount','Jumlah',['class'=>'col-md-2 control-label'])}}
	<div class="col-md-4">
		{{Form::number('amount',null,['class'=>'form-control','min'=>1])}}
		{!!$errors->first('amount','<p class="help-block"><strong>:message</strong> </p>') !!}
		@if(isset($model))
			<span class="help-block">
				<strong>{{$model->borrowed}} Borrowed</strong>
			</span>
		@endif
	</div>
</div>
<div class="form-group {{$errors->has('cover') ? 'has-error' : ''}}">
	{{Form::label('cover','cover',['class'=>'col-md-2 control-label'])}}
	<div class="col-md-4">

		{{Form::file('cover')}}
		{!!$errors->first('cover','<p class="help-block"><strong>:message</strong> </p>') !!}
		@if(isset($model))
		<img src="{{asset('img')}}/{{$model->cover}}" alt="">
		@endif
	</div>
</div>
<div class="form-group">
	<div class="col-md-4 col-md-offset-2">
			{{Form::submit(isset($model) ? 'Update' : 'Save' , ['class'=>'btn btn-info'])}}
	</div>
</div>