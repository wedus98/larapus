@extends('layouts.app')
@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-12">

                <div class="panel panel-default">
                    
                    <div class="panel-heading">
                        <h2 class="panel-title">Dashboard</h2>
                    </div>
                </div>
                <div class="panel-body">
                    <table class="table table-stripped" id="dataTable">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Book Title</th>
                                
                                <th>Action</th>
                            </tr>
                        </thead>
                        @php($no=1)
                        <tbody>
                            @foreach($borrowLog as $data)
                            <tr>
                                <td>{{$no++}}</td>
                                <td>{{$data->book->title}}</td>
                                <td>{{Form::model($data,['url'=>route('book.return',$data->book_id),
                                                        'method'=>'put',
                                                        'class'=>'form-inline',
                                                        'onsubmit'=>'return confirm("return this book?")'])}}
                                    {{Form::submit('Return',['class'=>'btn btn-warning'])}}
                                    {{Form::close()}}
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                        
                    </table>
                </div>
            </div>
        </div>
    </div>


@endsection

