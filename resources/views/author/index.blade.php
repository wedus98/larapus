@extends('layouts.app')
@section('content')

	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<ul class="breadcrumb">
					<li><a href="{{url('/home')}}">Dashboard</a></li>
					<li class="active">Penulis</li>
				</ul>
				<div class="panel panel-default">
					<div class="panel-heading">
						<h2 class="panel-title">Penulis</h2>
					</div>
				</div>
				<div class="panel-body">
					<p><a href="{{Url('admin/authors/create')}}" class="btn btn-success">Add Authors</a></p>
					<table class="table table-stripped" id="dataTable">
						<thead>
							<tr>
								<th>No</th>
								<th>Nama</th>
								<th>Action</th>
							</tr>
						</thead>
						@php($no=1)
						<tbody>
							@foreach($authors as $data)
							<tr>
								<td>{{$no++}}</td>
								<td>{{$data->name}}</td>
								<td>
									{{Form::model($data,['route'=>['admin.authors.destroy',$data],'method'=>'delete','class'=>'form-inline','onsubmit'=>'return confirm("Are you Sure ?")'])}}
									<a href="{{url('admin/authors')}}/{{$data->id}}/edit" class="btn btn-success">Edit</a>
									{{Form::submit('Delete',['class'=>'btn btn-danger'])}}
									{{Form::close()}}
								</td>


							</tr>
							@endforeach
						</tbody>
						
					</table>
				</div>
			</div>
		</div>
	</div>


@endsection

