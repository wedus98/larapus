

	<div class="form-group {{$errors->has('name') ? 'has-error' : ''}}">
			<label for="" class="col-md-2 control-label">
				Nama
			</label>
			<div class="col-md-4">
				{{Form::text('name',null,['class'=>'form-control'])}}
				{!!$errors->first('name','<p class="help-block">:message</p>')!!}
			</div>
	</div>

	<div class="form-group {{$errors->has('name') ? 'has-error' : ''}}">
			<div class="col-md-4 col-md-offset-2">
				{{Form::submit(isset($model) ? 'Edit' :'Add' ,['class'=>'btn btn-primary'])}}
			</div>
	</div>