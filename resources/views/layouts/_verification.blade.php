@if(session()->has('verification_notif.message'))


	<div class="container" >
		<div class="alert alert-{{session()->get('verification_notif.level')}}">
		 <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				{!! Session()->get('verification_notif.message')!!}
		</div>
	</div>
@endif