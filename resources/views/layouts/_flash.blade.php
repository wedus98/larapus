@if(session()->has('flash_notification.message'))


	<div class="container" id="notif">
		<div class="alert alert-{{session()->get('flash_notification.level')}}">

				{!! Session()->get('flash_notification.message')!!}
		</div>
	</div>
@endif