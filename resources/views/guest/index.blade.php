@extends('layouts.app')
@section('content')

	<div class="container">
		<div class="row">
			<div class="col-md-12">

				<div class="panel panel-default">
					
					<div class="panel-heading">
						<h2 class="panel-title">Books</h2>
					</div>
				</div>
				<div class="panel-body">
					<table class="table table-stripped" id="dataTable">
						<thead>
							<tr>
								<th>No</th>
								<th>Book Title</th>
								<th>Author</th>
								<td>Stock</td>
								<th>Action</th>
							</tr>
						</thead>
						@php($no=1)
						<tbody>
							@foreach($books as $data)
							<tr>
								<td>{{$no++}}</td>
								<td>{{$data->title}}</td>
								<td>{{$data->hasAuthor->name}}</td>
								<td>{{$data->stock}}</td>
								<td>
									@if(!Entrust::hasRole('admin'))
									<a href="{{url('book/')}}/{{$data->id}}/borrow" class="btn btn-danger" onclick="return confirm('are you sure?')">Borrow</a>
									@endif
								</td>


							</tr>
							@endforeach
						</tbody>
						
					</table>
				</div>
			</div>
		</div>
	</div>


@endsection

