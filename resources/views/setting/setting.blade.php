@extends('layouts.app')
@section('content')

	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<ul class="breadcrumb">
					<li><a href="{{url('/home')}}">Dashboard</a></li>
					<li class="active">Profile</li>
				</ul>
				<div class="panel panel-default">
					<div class="panel-heading">
						<h2 class="panel-title">Profile</h2>
					</div>
				</div>
				<div class="panel-body">
						{{Form::model(Auth::user(),['url'=>url('setting/profile'),'method'=>'POST','class'=>'form-horizontal'])}}
								<div class="form-group {{$errors->has('name') ? 'has-error' : ''}}">
										<label for="" class="col-md-2 control-label">
											Nama
										</label>
										<div class="col-md-4">
											{{Form::text('name',null,['class'=>'form-control'])}}
											{!!$errors->first('name','<p class="help-block">:message</p>')!!}
										</div>
								</div>
								<div class="form-group {{$errors->has('email') ? 'has-error' : ''}}">
										<label for="" class="col-md-2 control-label">
											Email
										</label>
										<div class="col-md-4">
											{{Form::email('email',null,['class'=>'form-control'])}}
											{!!$errors->first('email','<p class="help-block">:message</p>')!!}
										</div>
								</div>
								<div class="form-group">
										<div class="col-md-4 col-md-offset-2">
											{{Form::submit('Change' ,['class'=>'btn btn-primary'])}}
										</div>
								</div>
						{{Form::close()}}
				</div>
			</div>
		</div>
	</div>


@endsection

