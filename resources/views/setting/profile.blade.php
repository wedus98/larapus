@extends('layouts.app')
@section('content')

	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<ul class="breadcrumb">
					<li><a href="{{url('/home')}}">Dashboard</a></li>
					<li class="active">Profile</li>
				</ul>
				<div class="panel panel-default">
					<div class="panel-heading">
						<h2 class="panel-title">Profile</h2>
					</div>
				</div>
				<div class="panel-body">
						<table class="table">
							<thead>
								<tr>
									<td>Nama</td>
									<td>:</td>
									<td>{{Auth::user()->name}}</td>
								</tr>
								<tr>
									<td>Email</td>
									<td>:</td>
									<td>{{Auth::user()->email}}</td>
								</tr>
								<tr>
									<td></td>
									<td></td>
									<td><a href="{{url('setting/profile/edit')}}" class="btn btn-success">Change Profile</a></td>
								</tr>
							</thead>
						</table>
				</div>
			</div>
		</div>
	</div>


@endsection

