@extends('layouts.app')
@section('content')

	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<ul class="breadcrumb">
					<li><a href="{{url('/home')}}">Dashboard</a></li>
					<li class="active">Profile</li>
				</ul>
				<div class="panel panel-default">
					<div class="panel-heading">
						<h2 class="panel-title">Profile</h2>
					</div>
				</div>
				<div class="panel-body">
						{{Form::open(['url'=>url('setting/password'),'class'=>'form-horizontal'])}}

							<div class="form-group {{$errors->has('old_password') ? 'has-error' : ''}}">
										<label for="" class="col-md-2 control-label">
											Old Password
										</label>
										<div class="col-md-4">
											{{Form::password('old_password',['class'=>'form-control'])}}
											{!!$errors->first('old_password','<p class="help-block">:message</p>')!!}
										</div>
							</div>
							<div class="form-group {{$errors->has('new_password') ? 'has-error' : ''}}">
										<label for="" class="col-md-2 control-label">
											New Password
										</label>
										<div class="col-md-4">
											{{Form::password('new_password',['class'=>'form-control'])}}
											{!!$errors->first('new_password','<p class="help-block">:message</p>')!!}
										</div>
							</div>
							<div class="form-group {{$errors->has('new_password_confirmation') ? 'has-error' : ''}}">
										<label for="" class="col-md-2 control-label">
											Confirm Password
										</label>
										<div class="col-md-4">
											{{Form::password('new_password_confirmation',['class'=>'form-control'])}}
											{!!$errors->first('new_password_confirmation','<p class="help-block">:message</p>')!!}
										</div>
							</div>
							<div class="form-group">
										<div class="col-md-4 col-md-offset-2">
											{{Form::submit('Change' ,['class'=>'btn btn-primary'])}}
										</div>
								</div>

						{{Form::close()}}
				</div>
			</div>
		</div>
	</div>


@endsection

