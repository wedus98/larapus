<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Zizaco\Entrust\Traits\EntrustUserTrait;
use App\Book;
use App\BorrowLog;
use App\Exceptions\BookException;
use Auth;
use Illuminate\Support\Facades\Mail;


class User extends Authenticatable
{
    use EntrustUserTrait;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    protected $casts = ['is_verified'=>'boolean'];
    public function borrowLogs(){
        return $this->hasMany('App\BorrowLog');

    }

    public function borrow(Book $book){
        if($book->stock < 1 ){
            throw new BookException("Book Out of Stock");
        }
        if($this->borrowLogs()->where('book_id',$book->id)->where('is_returned',0)->count() > 0){
            throw new BookException("Book ".$book->title." Borrowed");
        }

        $borrowLog = BorrowLog::create([
                    'user_id'=>Auth::user()->id,
                    'book_id'=>$book->id
                    ]);
        return $borrowLog;
    }
    public function generateToken(){
        $token = $this->verification_token;
        if(!$token){
            $token = str_random(40);
            $this->verification_token = $token;
            $this->save();
        }
        return $token;
    }
    public function sendVerification(){
        $token = $this->generateToken();
        $user = $this;
        Mail::send('auth.emails.verification',compact('user','token'),function($m) use ($user){
            $m -> to($user->email,$user->name)->subject('Verification Account larapus');
        } );
    }
     public function verify(){
            $this->is_verified = 1;
            $this->verification_token = null;
            $this->save();
    }

}
