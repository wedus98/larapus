<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    //

	protected $fillable = ['title','amount','author_id','cover'];
    public $timestamps = false;

    public static function boot(){
    	parent::boot();
    	self::updating(function($book){
    			if($book -> amount < $book->borrowed){
    				 Session::flash("flash_notification",[
					                "level"=>'danger',
					                "message"=>"Amount Book ". $book->title . " must more than or equals ".$book->borrowed]);
    				 return false;
    			}
    	});
    	self::deleting(function($book){
    		if ($book->borrowLogs()->borrowed()->count() > 0) {
    			 Session::flash("flash_notification",[
					                "level"=>'danger',
					                "message"=>"Books Already borrowed or Books Borrowed "]);
    				 return false;
    		}
    	});
    }

    public function hasAuthor(){
    	return $this->belongsTo('App\Author','author_id');
    }
    public function borrowLogs(){
    	return $this->hasMany('App\BorrowLog');
    }
    public function getBorrowedAttribute(){
    	return $this->borrowLogs()->borrowed()->count();
    }
    public function getStockAttribute(){
     	///method borrowlogs -> scope di borrowlogs -> count function
     	$borrowed = $this->borrowLogs()->borrowed()->count();
     	$stock = $this->amount - $borrowed;
     	return $stock;   
    }
}
