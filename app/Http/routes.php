<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

					Route::get('/', 'GuestController@index');
					Route::get('auth/verify/{token}','Auth\AuthController@verify');	
					Route::get('auth/send-verification','Auth\AuthController@resend');


					Route::auth();
					Route::get('/home','HomeController@index');
					Route::get('setting/profile','SettingController@profile');
					Route::get('setting/profile/edit','SettingController@setProfile');
					Route::post('setting/profile','SettingController@updateProfile');
					Route::get('setting/changepassword','SettingController@editPassword');
					Route::post('setting/password','SettingController@updatePassword');
					Route::get('book/{id}/borrow', ['middleware'=>['auth','role:member'],
													'as'		=> 'book.borrow',
													'uses'		=> 'BookController@borrow']);
					Route::put('book/{id}/return',['middleware'=>['auth','role:member'],
													'as'	   => 'book.return',
													'uses'     => 'BookController@returnBack']);
					Route::group(['prefix'=>'admin','middleware'=>['auth','role:admin']],function(){

							Route::resource('authors','AuthorController');
							Route::resource('books','BookController');
					});



