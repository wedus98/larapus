<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use Session;

class UserShouldVerified
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $response =  $next($request);
        if(Auth::check() && !Auth::user()->is_verified){
        $link = url('auth/send-verification').'?email='.urlencode(Auth::user()->email);

                Auth::logout();
                 Session::flash("verification_notif",[
                "level"=>'warning',
                "message"=>"Your Account is not Active Check your email. <a class='alert-link' href='$link'>Resend Verification</a>"]);
                 return Redirect('/login');

        }
        return $response;
    }
}
