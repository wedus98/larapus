<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use App\Role;
use Illuminate\Http\Request;
use App\Http\Requests;
use Auth;
use Session;
class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware($this->guestMiddleware(), ['except' => 'logout']);
        $this->middleware('userVerified');
    }


    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
            'g-recaptcha-response'=>'required'
        ]);
    }

    protected function create(array $data)
    {
        $user = User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
        $memberRole = Role::where('name','member')->first();
        $user->attachRole($memberRole);
        $user->sendVerification();
        return $user;
    }
    public function verify(Request $request,$token){
            $email = $request->get('email');
            $user = User::where('verification_token',$token)->where('email',$email)->first();
            if($user){
                 $user->verify();
                 Session::flash("flash_notification",[
                "level"=>'success',
                "message"=>"Verification Success"]);
                 Auth::login($user);

            }
            return Redirect('/');
    }
    public function resend(Request $request){
        $user = User::where('email',$request->get('email'))->first();
        if($user && !$user->is_verified){
            $user->sendVerification();
             Session::flash("verification_notif",[
                "level"=>'success',
                "message"=>"Check Your Email for verification"]);
        }
        return Redirect('/login');

    }
}
