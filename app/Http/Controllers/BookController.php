<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use File;
use App\Http\Requests;
use App\Book;
use Session;
use App\Http\Requests\StoreBookRequest;
use App\Http\Requests\UpdateBookRequest;
use Auth;
use App\BorrowLog;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Exceptions\BookException;

class BookController extends Controller
{
    
    public function index()
    {
        //
        $books = Book::with('hasAuthor')->orderBy('id','DESC')->get();
        return View('book.index',compact('books'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //

        return View('book.create'); 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreBookRequest $request)
    {
        //

        $book = Book::create($request->except('cover'));

        if($request->hasFile('cover')){
            $uploadedCover = $request->file('cover');
            $extension = $uploadedCover->getClientOriginalExtension();
            $fileName = rand(1,1000)."_".date('Y-m-d').".".$extension;
            $path = public_path(). DIRECTORY_SEPARATOR . 'img';
            $uploadedCover->move($path,$fileName);
            $book->cover =  $fileName;
            $book->save();

        }
         Session::flash("flash_notification",[
                "level"=>'success',
                "message"=>"Add ". $book->title . " Success"]);

        return Redirect('admin/books');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $book = Book::findOrfail($id);
        return View('book.edit',compact('book'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateBookRequest $request, $id)
    {
        //
   
        $book = Book::findOrfail($id);
        $data = $request->only('title','author_id','amount');
        if(!$book->update($request->all())) return redirect()->back();
         if($request->hasFile('cover') == true){
            $path = public_path(). DIRECTORY_SEPARATOR . 'img';
            File::delete($path.DIRECTORY_SEPARATOR.$book->cover);
            $uploadedCover = $request->file('cover');
            $extension = $uploadedCover->getClientOriginalExtension();
            $fileName = rand(1,1000)."_".date('Y-m-d').".".$extension;
            $uploadedCover->move($path,$fileName);    
            $data['cover']=$fileName;
        }

        $book->update($data);
        Session::flash("flash_notification",[
                "level"=>'success',
                "message"=>"Update ". $book->title . " Success"]);
        return Redirect('admin/books');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $book = Book::findOrfail($id);
        if(!$book->delete()) return Redirect()->back();
        if($book->cover !==''){
            $path = public_path(). DIRECTORY_SEPARATOR . 'img';

            File::delete($path.DIRECTORY_SEPARATOR.$book->cover);
        }
        $book -> delete();
        Session::flash("flash_notification",[
                "level"=>'success',
                "message"=>"Delete Success"]);
        return Redirect('admin/books');


    }
    public function borrow($id){

        try {
                $book = Book::findOrfail($id);
                Auth::user()->borrow($book);
                Session::flash("flash_notification",[
                "level"=>'success',
                "message"=>"Borrow ".$book->title." Success"]);

        }catch (BookException $e){
                 Session::flash("flash_notification",[
                "level"=>'danger',
                "message"=>$e->getMessage()]);
        } 
        catch (ModelNotFoundException $e) {
                Session::flash("flash_notification",[
                "level"=>'danger',
                "message"=>"Book Not Found"]);
            
        }
        return Redirect('/');
    }

    public function returnBack($id){
        $BorrowLog = BorrowLog::where('user_id',Auth::user()->id)
                                ->where('book_id',$id)
                                ->where('is_returned',0)
                                ->first();
        if($BorrowLog){
            $BorrowLog -> is_returned = true;
            $BorrowLog ->  save();
             Session::flash("flash_notification",[
                "level"=>'success',
                "message"=>"Return ".$BorrowLog->book->title." Success"]);
        }
        return Redirect('/home');
    }
}
