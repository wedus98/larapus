<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Entrust;
use App\BorrowLog;
use Auth;
class HomeController extends Controller
{
    
    public function index()
    {
        if(Entrust::hasRole('admin')) return $this->adminView();
        if(Entrust::hasRole('member')) return $this->memberView();
        return view('home');
    }
    protected function adminView(){
        return view('dashboard.admin');
    }
    protected function memberView(){
        $borrowLog = Auth::user()-> borrowLogs()->borrowed()->get();
        return View('dashboard.member',compact('borrowLog'));
    }
}
