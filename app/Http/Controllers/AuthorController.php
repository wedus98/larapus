<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Author;
use Session;

class AuthorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $rules = ['name'=>'required|min:6'];

    public function index()
    {
        //
        $authors = Author::select(['name','id'])->get();

        return View('author.index',compact('authors'));

        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return View('author.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request,$this->rules);

      $author = Author::create($request->all());
        Session::flash("flash_notification",[
                "level"=>'success',
                "message"=>"Add ". $author->name . " Success"]);

        return Redirect('admin/authors');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $author = Author::findOrfail($id);
        return View('author.edit',compact('author'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $this->validate($request,$this->rules);
        $author = Author::findOrfail($id);
        $author->update($request->all());
        Session::flash("flash_notification",[
                "level"=>'success',
                "message"=>"Update ". $author->name . " Success"]);
        return Redirect('admin/authors');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $author= Author::findOrfail($id);

        $author->delete();
         Session::flash("flash_notification",[
                "level"=>'danger',
                "message"=>"Delete Success"]);
        return Redirect('admin/authors');
    }
}
