<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Http\Requests;
use Session;

class SettingController extends Controller
{
    //

    public function __construct(){
    	return $this->middleware('auth');
    }
    public function profile(){

    	return View('setting.profile');
    }
    public function setProfile(){
    	return View('setting.setting');
    }
    public function updateProfile(Request $request){
    	$user = Auth::user();
    	$this->validate($request,['name'=>'required|',
    							  'email'=>'required|email']);
    	$user->name = $request->get('name');
    	$user->email = $request->get('email');
    	$user->save();
    	Session::flash("flash_notification",[
                "level"=>'success',
                "message"=>"Update Profile Success"]);
    	return Redirect('setting/profile');

    }
    public function editPassword(){
    	return View('setting.changePassword');
    }
    public function updatePassword(Request $request){
    	$user = Auth::user();
    	$this->validate($request,['old_password'=>'required|passcheck:'.$user->password,
    							  'new_password'=>'required|confirmed|min:6'],
    							  ['old_password.passcheck'=>'Old password false']);
    	$user->password = bcrypt($request->get('new_password'));
    	$user->save();
    	Session::flash("flash_notification",[
                "level"=>'success',
                "message"=>"Change Password Success"]);
    	return Redirect('setting/changePassword');
    }
}
