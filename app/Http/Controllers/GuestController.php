<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Book;
class GuestController extends Controller
{
    //
    public function index(Request $Request){

    		$books = Book::with('hasAuthor')->get();

    		return View('guest.index',compact('books'));
    }
}
