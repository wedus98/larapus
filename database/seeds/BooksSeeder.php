<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Author;
use App\Book;
use Faker\Factory as Faker;
class BooksSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        DB::table('books')->truncate();
        DB::table('authors')->truncate();

    	

    	$faker = Faker::create();
    	$books = [];
        $authors= [];
    	foreach (range(1,100) as $index ) {
    		
    		$books[]= ['title'=> $faker->name,
    				   'author_id'=> rand(1,300),
    				   'amount'=>rand(1,10)

    					   ];	
    	};
        foreach(range(1,300) as $index ){
            $authors[] = ['name'=>$faker->name];
        }
        DB::table('authors')->insert($authors);
    	DB::table('books')->insert($books);

    }
}
